package com.phichet.robot;

public class Unit {
    private char symbol;
    private int x;
    private int y;
    private boolean isDominate;
    private Map map;

    public Unit(Map map,char symbol,int x,int y,boolean isDominate){
        this.map = map;
        this.symbol = symbol;
        this.x = x;
        this.y = y;
        this.isDominate = isDominate;
    }
    public boolean isOn(int x,int y){
        return this.x == x && this.y == y;
    }
    public boolean setX(int x){
        if(!map.isInWidth(x))return false;
        if(map.hasDominate(x, y))return false;
        this.x = x;
        return true;
    }
    public boolean setY(int y){
        if(!map.hasDominate(x,y))return false;
        this.y = y;
        return true;
    }
    public boolean setXY(int x, int y){
        if(!map.hasDominate(x,y))return false;
        this.x = x;
        this.y = y;
        return true;
    }
    public int getX(){
        return x;
    }
    public int getY(){
        return y;
    }
    public boolean isDominate(){
        return isDominate;
    }
    public Map getMap(){
        return map;
    }
    public char getSymbol(){
        return symbol;
    }
    public String toString(){
        return "Unit(" + this.symbol + ")" + "[" + this.x + ", " + this.y + "] is on " + map;
    }
}
